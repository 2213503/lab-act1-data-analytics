import math
import statistics
import numpy as np
import pandas as pd
from decimal import Decimal as D
from scipy import stats

# Compute the mean/average of the given data
mean_statistics = statistics.mean([64.79,9.97,7.75,0,5.88,10.03,0.22,0.7,0.07,0.55,0,0,0,0.03,0,0,0.01,0,0,0,0,0])
data = ([64.79,9.97,7.75,0,5.88,10.03,0.22,0.7,0.07,0.55,0,0,0,0.03,0,0,0.01,0,0,0,0,0])
mean_manual = sum(data) / len(data)
mean_decimal = statistics.mean([D("0.5"), D("0.75"), D("0.625"), D("0.375")])
mean_floats = statistics.fmean([1.5, 2.0, 2.8, 0.9, 2.25])

# Median high
median_high_odd = statistics.median_high([7, 3, 6, 5, 2, 1])
median_high_even = statistics.median_high([4, 3, 6, 5, 2, 1, 7])

# Mode
mode_strings = statistics.mode(["apple", "orange", "apple", "grapes", "grapes", "apple", "apple"])
mode_numbers = statistics.mode([20, 15, 12, 17, 18, 15, 20, 20, 15])
multimode_numbers = statistics.multimode([20, 15, 12, 17, 18, 15, 20, 20, 15])

# Handling NaN values
x = [1.0, 3, 2, 4.5, 8.0]
x_with_nan = [1.0, 3, 2, math.nan, 4.5, 8.0]
mean_without_nan = statistics.mean(x)
mean_with_nan = statistics.mean(x_with_nan)

# Numpy array operations
vector_row = np.array([1, 2, 3])
vector_column = np.array([[1], [2], [3]])

a = np.array([1, 2, 3, 4])
mean_np = np.mean(a)

# Pandas Series
data_series = pd.Series([10, 5, 2, 6, 7, 8, 4])

# Pandas DataFrame
covid_dict = {'Cebu': 7800, 'Davao': 8000, 'Manila': 10000, 'Baguio': 6500, 'Cavite': 2000}
covid_series = pd.Series(covid_dict)
covid_df = pd.DataFrame({'number_of_cases': covid_series})

# Descriptive statistics for DataFrame
df = pd.DataFrame({"A": [1, 2, 3, 4, 5], "B": [15, 12, 54, 13, 12], "C": [2, 6, 7, 3, 8], "D": [14, 23, 17, 21, 16]})
mean_rows = df.mean()
mean_columns = df.mean(axis=0)
mean_rows_axis1 = df.mean(axis=1)

# Variance and Standard Deviation
variance_sample = statistics.variance([3, 5, 7, 2])
variance_np = np.var([3, 5, 7, 2], ddof=1)
variance_pandas = data_series.var(ddof=1)

std_sample = statistics.stdev(x)
std_np = np.std(y, ddof=1)
std_pandas = data_series.std(ddof=1)

# Range
range_np = np.ptp(x)

# Percentiles
percentiles_np = np.percentile(a, [25, 50, 75])

# Interquartile range
quartiles_np = np.quantile(y, [0.25, 0.75])
iqr = quartiles_np[1] - quartiles_np[0]

# Reading CSV file with pandas
df_csv = pd.read_csv("DESKTOP/SAMPLE.CSV")
df_class = pd.read_csv("DESKTOP/CLASS.CSV")

# Skip rows while reading CSV
df_skip_rows = pd.read_csv("DESKTOP/SAMPLE.CSV", skiprows=[2, 4])

# Quantiles and Descriptive Statistics for DataFrame
quantiles_df = df_class.quantile([0.25, 0.5, 0.75], axis=0)
summary_stats_df = df_class.describe()

# Variance, Standard Deviation, and Range for DataFrame
variance_df = df_class.var()
std_df = df_class.std()
range_df = np.ptp(df_class)

# Histogram and Pivot Table
df_resto = pd.read_csv('desktop/resto1.csv')
plot_histogram = df_resto['Count'].plot.hist(title='Histogram of Count')
table_pivot_sum = pd.pivot_table(df_resto, values='Count', index=['Color'], columns=['Size'], aggfunc=np.sum)
table_pivot_mean = pd.pivot_table(df_resto, values='Count', index=['Color'], columns=['Size'], aggfunc=np.mean)